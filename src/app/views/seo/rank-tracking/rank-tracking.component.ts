import { Component, OnInit } from '@angular/core';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { RankTrackingService } from '../../../service/data/rank-tracking.service';
import { RankTrackingKeyword } from '../../../../models/rankTrackingKeyword.model';
import { Subject } from 'rxjs';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { RankTrackingLog } from '../../../../models/rankTrackingLog.model';
import { Router } from '@angular/router';
import { RankTrackingSumary } from '../../../../models/rankTrackingSumary.model';
import { ReqRankTrackingKeywordGraph } from '../../../../models/reqGraphRankTracking.model';
import { RankTrackingKeywordGraph } from '../../../../models/rankTrackingKeywordGraph.model';
import { TopKeyword } from '../../../../models/topKeyWordData.model';
// import { resolve } from 'path';
// import { Http, Response } from '@angular/http';
// import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-rank-tracking',
  templateUrl: './rank-tracking.component.html',
  styleUrls: ['./rank-tracking.component.css']
})
export class RankTrackingComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<RankTrackingKeyword> = new Subject();

  dtOptionsLog: DataTables.Settings = {};
  dtOptionsLogGraph: DataTables.Settings = {};
  dtOptionsTopData: DataTables.Settings = {};
  // dtTriggerLog: Subject<RankTrackingLog> = new Subject();

  rankTrackingKeywords: RankTrackingKeyword[] = [];
  rankTrackingLog: RankTrackingLog[] = [];
  rankTrackingTopKeyword: RankTrackingKeyword[] = [];
  rankTrackingLogGraph: RankTrackingLog[] = [];
  topKwyword = new TopKeyword()
  rankTrackingSumary = new RankTrackingSumary;
  traficGraph = new RankTrackingKeywordGraph();
  closeResult: string;
  urlPage = '';
  viewDetail = false;
  keyword = '';
  keywordId = -1;
  newKeyword = '';
  unvalid = false;
  notDisable = true;
  statusOpenTopData = false;
  viewDetailGraph = false;
  public mainChartElements = 30;
  chartReady = false;
  maxCeil = 30;
  dialogTop = '';

  public mainChartData1: Array<number> = [];

  public mainChartData: Array<any> = [
    {
      data: this.mainChartData1,
      label: 'Rank'
    }
  ];
  /* tslint:disable:max-line-length */
  public mainChartLabels: Array<any> = [];
  /* tslint:enable:max-line-length */
  public mainChartOptions: any;

  public mainChartColours: Array<any> = [
    { // brandInfo
      backgroundColor: hexToRgba(getStyle('--info'), 10),
      borderColor: getStyle('--info'),
      pointHoverBackgroundColor: '#fff'
    }
  ];
  public mainChartLegend = false;
  public mainChartType = 'line';

  constructor(
    private rankTrackingKeywordService: RankTrackingService,
    private modalService: NgbModal,
    private router: Router

  ) {
    for (let index = 0; index < this.mainChartLabels.length; index++) {
      this.mainChartLabels[index] = '';

    }
  }

  ngOnInit(): void {
    this.getRankTrackingKeyword();
    this.getSumary();
    this.getTopKeyword();

  }
  getTopKeyword() {
    this.rankTrackingKeywordService.getTopKeyword(Number(sessionStorage.getItem('activeDomain'))).subscribe(
      response => {
        this.topKwyword = response;
      }
    );
  }

  viewTop3(content) {
    this.dialogTop = '3';
    this.openLgTopData(content);
    this.getDataTop3();
  }
  getDataTop3() {
    this.rankTrackingKeywordService.getTop3KeywordList(Number(sessionStorage.getItem('activeDomain'))).subscribe(
      response => {
        this.statusOpenTopData = true;
        this.rankTrackingTopKeyword = response;
        this.dtOptionsTopData = {
          pagingType: 'full_numbers'
        };
      }
    );
  }
  viewTop5(content) {
    this.dialogTop = '5';
    this.openLgTopData(content);
    this.getDataTop5();
  }
  getDataTop5() {
    this.rankTrackingKeywordService.getTop5KeywordList(Number(sessionStorage.getItem('activeDomain'))).subscribe(
      response => {
        this.statusOpenTopData = true;
        this.rankTrackingTopKeyword = response;
        this.dtOptionsTopData = {
          pagingType: 'full_numbers'
        };
      }
    );
  }
  viewTop10(content) {
    this.dialogTop = '3';
    this.openLgTopData(content);
    this.getDataTop10();
  }
  getDataTop10() {
    this.rankTrackingKeywordService.getTop10KeywordList(Number(sessionStorage.getItem('activeDomain'))).subscribe(
      response => {
        this.statusOpenTopData = true;
        this.rankTrackingTopKeyword = response;
        this.dtOptionsTopData = {
          pagingType: 'full_numbers'
        };
      }
    );
  }


  openLgTopData(content) {
    
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', windowClass: 'my-class' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      this.statusOpenTopData = false;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.statusOpenTopData = false;
    });
  }




  getRankTrackingKeyword() {
    let idDomain = Number(sessionStorage.getItem('activeDomain'));
    this.rankTrackingKeywordService.getKeywords(idDomain).subscribe(
      response => {
        this.rankTrackingKeywords = response;
        // console.log(this.rankTrackingKeywords);
        this.dtOptions = {
          pagingType: 'full_numbers'
        };
        this.dtTrigger.next();
      }
    );
  }
  getRankTrackingLongs(idKeyword: number) {
    this.rankTrackingKeywordService.getLogs(idKeyword).subscribe(
      response => {
        this.rankTrackingLog = response;
        this.viewDetail = true;
        this.dtOptionsLog = {
          pagingType: 'full_numbers'
        };
        console.log(response);
      }
    );
  }
  getRankTrackingLongsGraph(req: ReqRankTrackingKeywordGraph) {
    this.rankTrackingKeywordService.getLogsGraph(req).subscribe(
      response => {
        this.rankTrackingLogGraph = response;
        this.viewDetailGraph = true;
        this.dtOptionsLogGraph = {
          pagingType: 'full_numbers'
        };
      }
    );
  }

  viewData(idKeyword: number, keyword: string, content) {
    // console.log('id View = '+idKeyword)

    this.keyword = keyword;
    this.openLg(idKeyword, content);

  }
  deleteData(idKeyword: number, keyword: string, content) {
    // console.log('id Delete = '+idKeyword)
    this.openSm(content);
    this.keywordId = idKeyword;
    this.keyword = keyword;
  }
  deleteKeyword() {
    this.rankTrackingKeywordService.deleteKeyword(this.keywordId).subscribe(
      res => {
        this.modalService.dismissAll();
        // refresh curent router 
        this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
          this.router.navigate(['seo/rank-tracking']));
      }
    );
  }


  updateData() {
    this.notDisable = false;
    this.rankTrackingKeywordService.updateKeyword(Number(sessionStorage.getItem('activeDomain'))).subscribe(
      res => {
        this.modalService.dismissAll();
        // refresh curent router 
        this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
          this.router.navigate(['seo/rank-tracking']));

      }
    );
  }

  getSumary() {
    this.rankTrackingKeywordService.getSumary(Number(sessionStorage.getItem('activeDomain'))).subscribe(
      response => {
        this.rankTrackingSumary = response;
      }
    );
  }

  addKeyword(content) {
    this.openSm(content);
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  saveKeyword() {
    if (this.validate()) {

      this.rankTrackingKeywordService.addKeyword(
        new RankTrackingKeyword(-1, this.newKeyword, 0, 0, 0, null, Number(sessionStorage.getItem('activeDomain')))).subscribe(
          response => {
            console.log(response);
            this.modalService.dismissAll();

            // refresh curent root
            this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
              this.router.navigate(['seo/rank-tracking']));
          }
        );
    } else {
      this.unvalid = true;
    }
  }
  validate() {
    if (this.newKeyword === '') {
      return false;
    } else {
      return true;
    }
  }

  viewGraph(page: string, keyword_id: number, content) {
    this.urlPage = page;
    this.openSubLg(new ReqRankTrackingKeywordGraph(page, keyword_id), content);
    this.getRankTrackingLongsGraph(new ReqRankTrackingKeywordGraph(page, keyword_id));
  }

  getGraph(req: ReqRankTrackingKeywordGraph) {
    this.rankTrackingKeywordService.getKeywordGraph(req).subscribe(
      response => {
        this.traficGraph = response;
        for (let index = 0; index < this.traficGraph.value.length; index++) {
          this.mainChartData1[index] = this.traficGraph.value[index];

        }
        this.mainChartLabels = this.traficGraph.date;
        // this.maxCeil = this.traficAnalystSumary.maxValue;
        this.setProperty(this.traficGraph.maxValue);
        this.chartReady = true;
        // console.log('result')
        // console.log(response);
        // console.log(this.traficGraph)
      }
    );
  }
  
  // clearDataGraph(){
  //   this.setProperty(0);
  // }
  //this is for modal
  openSubLg(req: ReqRankTrackingKeywordGraph, content) {
    this.getGraph(req);

    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', windowClass: 'my-class' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      this.viewDetailGraph = false;
      this.chartReady = false;
      // this.clearDataGraph();
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;

    });
  }
  openLg(idKeyword: number, content) {
    this.getRankTrackingLongs(idKeyword)
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', windowClass: 'my-class' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      this.viewDetail = false;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.viewDetail = false;
    });
  }
  openSm(content) {
    // this.getRankTrackingLongs(idKeyword)
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }


  private getDismissReason(reason: any): string {
    // this.dtTriggerLog.unsubscribe();

    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }



  }

  setProperty(maxCeil: number) {
    this.mainChartOptions = {
      tooltips: {
        enabled: false,
        custom: CustomTooltips,
        intersect: true,
        mode: 'index',
        position: 'nearest',
        callbacks: {
          labelColor: function (tooltipItem, chart) {
            return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor };
          }
        }
      },
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        xAxes: [{
          gridLines: {
            drawOnChartArea: false,
          },
          ticks: {
            callback: function (value: any) {
              return value;
            }
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true,
            maxTicksLimit: 5,
            stepSize: Math.ceil(maxCeil / 5),
            max: maxCeil
          }
        }]
      },
      elements: {
        line: {
          borderWidth: 2
        },
        point: {
          radius: 0,
          hitRadius: 10,
          hoverRadius: 4,
          hoverBorderWidth: 3,
        }
      },
      legend: {
        display: false
      }
    }
  }


}
