import { Component, OnInit } from '@angular/core';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { TraficAnalystService } from '../../../service/data/trafic-analyst.service';
import { TraficPage } from '../../../../models/traficAnalystPage.model';
import { Subject } from 'rxjs';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { RequestPageDetaial } from '../../../../models/reqPageTrafic.model';
import { PageTraficDetail } from '../../../../models/pageTrafic.model';
import { TraficPageKeyword } from '../../../../models/traficAnalystPageKeyword.model';
import { GoogleConsoleService } from '../../../service/data/google-console.service';
import { Router } from '@angular/router';
import { TraficAnalystSumary } from '../../../../models/traficAnalystSumary.model';
import { DeviceTrafic } from '../../../../models/traficDevice.model';
@Component({
  selector: 'app-trafic-analysis',
  templateUrl: './trafic-analysis.component.html',
  styleUrls: ['./trafic-analysis.component.css']
})
export class TraficAnalysisComponent implements OnInit {
  
  radioModel: string = 'Month';
  nowTime = new Date();
  devicetrafic = new DeviceTrafic();
  // mainChart

  public mainChartElements = 30;
  // maxCeil = 30;
  public mainChartData1: Array<number> = [];
  public mainChartData2: Array<number> = [];
  public mainChartData3: Array<number> = [];
  public mainChartData4: Array<number> = [];

  public mainChartData: Array<any> = [
    {
      data: this.mainChartData1,
      label: 'Trafic'
    },
    {
      data: this.mainChartData2,
      label: 'Click'
    },
    {
      data: this.mainChartData3,
      label: 'CTR(%)'
    },
    {
      data: this.mainChartData4,
      label: 'Average Trafic'
    }
  ];
  /* tslint:disable:max-line-length */
  public mainChartLabels: Array<any> = [];
  /* tslint:enable:max-line-length */
  public mainChartOptions: any;

  public mainChartColours: Array<any> = [
    { // brandInfo
      backgroundColor: hexToRgba(getStyle('--info'), 10),
      borderColor: getStyle('--info'),
      pointHoverBackgroundColor: '#fff'
    },
    { // brandSuccess
      backgroundColor: 'transparent',
      borderColor: getStyle('--success'),
      pointHoverBackgroundColor: '#fff'
    },
    { // brandInfo
      backgroundColor: 'transparent',
      borderColor: getStyle('--warning'),
      pointHoverBackgroundColor: '#fff'
    },
    { // brandDanger
      backgroundColor: 'transparent',
      borderColor: getStyle('--danger'),
      pointHoverBackgroundColor: '#fff',
      borderWidth: 1,
      borderDash: [8, 5]
    }
  ];
  public mainChartLegend = false;
  public mainChartType = 'line';

  public random(min: number, max: number) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  constructor(
    private traficAnalystService: TraficAnalystService,
    private modalService: NgbModal,
    private googleService: GoogleConsoleService,
    private router: Router
  ) { }
  notDisable = true;
  traficPages: TraficPage[] = [];
  detailTraficPage: PageTraficDetail[] = [];
  pageTraficKeyword: TraficPageKeyword[] = [];
  traficAnalystSumary: TraficAnalystSumary;
  dtOptions: DataTables.Settings = {};
  dtOptionsDetail: DataTables.Settings = {};
  dtOptionsKeyword: DataTables.Settings = {};
  dtTrigger: Subject<TraficPage> = new Subject();
  closeResult: string;
  vievDetail = false;
  viewKeyword = false;
  url = '';
  chartReady = false;

  ngOnInit(): void {

    this.getDataSumary();
    this.getDataTraficPage();
    this.getDeviceTrafic();
  }
  updateData() {
    this.notDisable = false;
    this.googleService.updateData().subscribe(
      response => {
        console.log(response)
        // refresh curent router 
        this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
          this.router.navigate(['seo/trafic']));
      }
    );

  }
  getDataTraficPage() {
    // let idDomain = Number(sessionStorage.getItem('activeDomain'));
    this.traficAnalystService.getTraficPage(Number(sessionStorage.getItem('activeDomain'))).subscribe(
      response => {
        this.traficPages = response;
        // console.log(this.traficPages);
        this.dtOptions = {
          pagingType: 'full_numbers'
        };
        this.dtTrigger.next();
      }
    );
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  openLg(page: string, content) {
    // this.getRankTrackingLongs(idKeyword)
    // this.getDetailPage(page);
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', windowClass: 'my-class' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      this.vievDetail = false;
      this.viewKeyword = false;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.vievDetail = false;
      this.viewKeyword = false;
    });
  }


  private getDismissReason(reason: any): string {
    // this.dtTriggerLog.unsubscribe();

    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  viewData(page: string, content) {
    this.openLg(page, content);
    this.getDetailPage(page);
    this.url = page;
  }
  viewKeywordDetail(page: string, content) {
    this.openLg(page, content);
    this.getPageKeywordTrafic(page);
    this.url = page;
  }

  getDetailPage(page: string) {
    this.traficAnalystService.getDetailPageTrafic(new RequestPageDetaial(page, Number(sessionStorage.getItem('activeDomain')))).subscribe(
      response => {
        this.detailTraficPage = response;
        this.vievDetail = true;
        this.dtOptionsDetail = {
          pagingType: 'full_numbers'
        };
      }
    );
  }
  getPageKeywordTrafic(page: string) {
    this.traficAnalystService.getTraficPageKeyword(new RequestPageDetaial(page, Number(sessionStorage.getItem('activeDomain')))).subscribe(
      response => {
        this.pageTraficKeyword = response;
        this.viewKeyword = true;
        this.dtOptionsKeyword = {
          pagingType: 'full_numbers'
        };
      }

    );
  }
  getDataSumary() {
    this.traficAnalystService.getSumary(Number(sessionStorage.getItem('activeDomain'))).subscribe(
      response => {
        this.traficAnalystSumary = response;
        for (let index = 0; index < this.traficAnalystSumary.trafic.length; index++) {
          this.mainChartData1.push(this.traficAnalystSumary.trafic[index]);

        }
        for (let index = 0; index < this.traficAnalystSumary.click.length; index++) {
          this.mainChartData2.push(this.traficAnalystSumary.click[index]);
        }
        for (let index = 0; index < this.traficAnalystSumary.ctr.length; index++) {
          this.mainChartData3.push(this.traficAnalystSumary.ctr[index]/100);
        }
        // this.mainChartData2 = this.traficAnalystSumary.ctr;

        for (let index = 0; index < this.traficAnalystSumary.click.length; index++) {
          this.mainChartData4.push(this.traficAnalystSumary.averageTrafic);
        }
        this.mainChartLabels = this.traficAnalystSumary.dates;
        // this.maxCeil = this.traficAnalystSumary.maxValue;
        this.setProperty(this.traficAnalystSumary.maxValue);
        this.chartReady = true;
        console.log(this.traficAnalystSumary);
      }
    );
  }
  getDeviceTrafic(){
    this.traficAnalystService.getDevice(Number(sessionStorage.getItem('activeDomain'))).subscribe(
      response =>{
        this.devicetrafic = response;
      }
    );
  }
  setProperty(maxCeil: number){
   this.mainChartOptions = {
      tooltips: {
        enabled: false,
        custom: CustomTooltips,
        intersect: true,
        mode: 'index',
        position: 'nearest',
        callbacks: {
          labelColor: function (tooltipItem, chart) {
            return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor };
          }
        }
      },
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        xAxes: [{
          gridLines: {
            drawOnChartArea: false,
          },
          ticks: {
            callback: function (value: any) {
              return value;
            }
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true,
            maxTicksLimit: 5,
            stepSize: Math.ceil(maxCeil / 5),
            max: maxCeil
          }
        }]
      },
      elements: {
        line: {
          borderWidth: 2
        },
        point: {
          radius: 0,
          hitRadius: 10,
          hoverRadius: 4,
          hoverBorderWidth: 3,
        }
      },
      legend: {
        display: false
      }
    }
  }
    

}
