import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RankTrackingComponent } from './rank-tracking/rank-tracking.component';
import { Routes, RouterModule } from '@angular/router';
import { TraficAnalysisComponent } from './trafic-analysis/trafic-analysis.component';
import { KeyWordPlanerComponent } from './key-word-planer/key-word-planer.component';
import { BacklinkComponent } from './backlink/backlink.component';
import { AuthGuardService } from '../../service/auth/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'SEO'
    },
    children: [
      {
        path: '',
        redirectTo: 'rank-tracking'
      },
      {
        path: 'rank-tracking',
        component: RankTrackingComponent,
        data: {
          title: 'Rank Tracking'
        }
      },
      {
        path: 'trafic',
        component: TraficAnalysisComponent,
        data: {
          title: 'Trafic Analyst'
        }
      },
      {
        path: 'keyword-planer',
        component: KeyWordPlanerComponent,
        data: {
          title: 'Keyword Planner'
        }
      },
      {
        path: 'backlink',
        component: BacklinkComponent,
        data: {
          title: 'BackLink'
        }
      }
    ],
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SeoRoutingModule { }
