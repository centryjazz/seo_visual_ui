import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { BackLinkSite } from '../../../../models/backlinkSite.model';
import { BackLinkTarget } from '../../../../models/backlinkTarget.model';
import { BackLinkText } from '../../../../models/backlinkText.model';
import { BackLinkService } from '../../../service/data/back-link.service';
import { Router } from '@angular/router';
import { BackLinkSumary } from '../../../../models/backlinkSumary.model';

@Component({
  selector: 'app-backlink',
  templateUrl: './backlink.component.html',
  styleUrls: ['./backlink.component.css']
})
export class BacklinkComponent implements OnInit {
  dtOptionsSite: DataTables.Settings = {};
  dtTriggerSite: Subject<BackLinkSite> = new Subject();
  dtOptionsTarget: DataTables.Settings = {};
  dtTriggerTarget: Subject<BackLinkTarget> = new Subject();
  dtOptionsKeyword: DataTables.Settings = {};
  dtTriggerKeyword: Subject<BackLinkText> = new Subject();
  backLinkSite: BackLinkSite[] = [];
  backLinkTarget: BackLinkTarget[] = [];
  backLinkKeyword: BackLinkText[] = [];
  backLinkSumary: BackLinkSumary;
  sumaryStatus = false;

  notDisable = true;
  constructor(
    private backlinkService: BackLinkService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getSumary();
    this.getData();

  }

  getData() {
    this.backlinkService.getBackLinkSite(Number(sessionStorage.getItem('activeDomain'))).subscribe(
      response => {
        this.backLinkSite = response;
        this.dtOptionsSite = {
          pagingType: 'full_numbers'
        };
        this.dtTriggerSite.next();
      }
    );
    this.backlinkService.getBackLinkTarget(Number(sessionStorage.getItem('activeDomain'))).subscribe(
      response => {
        this.backLinkTarget = response;
        this.dtOptionsTarget = {
          pagingType: 'full_numbers'
        };
        this.dtTriggerTarget.next();
      }
    );
    this.backlinkService.getBackLinkText(Number(sessionStorage.getItem('activeDomain'))).subscribe(
      response => {
        this.backLinkKeyword = response;
        this.dtOptionsKeyword = {
          pagingType: 'full_numbers'
        };
        this.dtTriggerKeyword.next();
      }
    );
  }
  updateData() {
    this.notDisable = false;
    this.backlinkService.updateBackLinkSite(Number(sessionStorage.getItem('activeDomain'))).subscribe(
      response => {
        console.log(`UpdateSite : ` + response);
        this.backlinkService.updateBackLinkTarget(Number(sessionStorage.getItem('activeDomain'))).subscribe(
          response => {
            console.log(`UpdateTarget : ` + response);
            this.backlinkService.updateBackLinkText(Number(sessionStorage.getItem('activeDomain'))).subscribe(
              response => {
                console.log(`UpdateKeyord : ` + response);
                this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
                  this.router.navigate(['seo/backlink']));
              }
            );
          }
        );
      }
    );
  }

  getSumary() {
    this.backlinkService.getSumary(Number(sessionStorage.getItem('activeDomain'))).subscribe(
      response => {
        this.sumaryStatus = true;
        this.backLinkSumary = response;
      }
    );
  }

}
