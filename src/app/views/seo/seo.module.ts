import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SeoRoutingModule } from './seo-routing.module';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { RankTrackingComponent } from './rank-tracking/rank-tracking.component';
import { TraficAnalysisComponent } from './trafic-analysis/trafic-analysis.component';
import { BacklinkComponent } from './backlink/backlink.component';
import { KeyWordPlanerComponent } from './key-word-planer/key-word-planer.component';
import { ChartsModule } from 'ng2-charts';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { DataTablesModule } from 'angular-datatables';
// import { Http, Response } from '@angular/http';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SeoRoutingModule,
    BsDropdownModule.forRoot(),
    TabsModule,
    CarouselModule.forRoot(),
    CollapseModule.forRoot(),
    PaginationModule.forRoot(),
    PopoverModule.forRoot(),
    ProgressbarModule.forRoot(),
    TooltipModule.forRoot(),
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    DataTablesModule
  ],
  declarations: [
    RankTrackingComponent,
    TraficAnalysisComponent,
    BacklinkComponent,
    KeyWordPlanerComponent
  ]
})
export class SeoModule { }
