import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { KeyWordPlaner } from '../../../../models/keywordPlaner.model';
import { KeyWordPlanerHistory } from '../../../../models/keywordPlanerHistory.model';
import { KeywordPlanerService } from '../../../service/data/keyword-planer.service';
import { Subject } from 'rxjs';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { UpdateStatus } from '../../../../models/updateStatus.model';
import { RankTrackingKeywordGraph } from '../../../../models/rankTrackingKeywordGraph.model';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { RelatedKeywords } from '../../../../models/RelatedKeyword.model';
@Component({
  // changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-key-word-planer',
  templateUrl: './key-word-planer.component.html',
  styleUrls: ['./key-word-planer.component.css']
})
export class KeyWordPlanerComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<KeyWordPlaner> = new Subject();

  dtOptionsRelatedKeyword: DataTables.Settings = {};
  dtTriggerRelatedKeyword: Subject<RelatedKeywords> = new Subject();

  dtOptionsHistory: DataTables.Settings = {};
  keyWordPlanner: KeyWordPlaner[] = [];
  historyGraph = new RankTrackingKeywordGraph();
  keyWordPlanerHistory: KeyWordPlanerHistory[] = [];
  relatedKeyword : RelatedKeywords[] = [];

  historyStatus = false;
  notInUpdate = true;
  viewDetail = false;
  addData = false;
  viewRelatedKeyword = false;
  closeResult = '';
  keyword = '';
  relatedSearch = '';
  addKeyword = '';
  updateStatus = new UpdateStatus();
  chartReady = false;
  warna = '';
  constructor(
    private keywordPlanerService: KeywordPlanerService,
    private modalService: NgbModal,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getDataKeyWordPlanner(Number(sessionStorage.getItem('activeDomain')));
  }
  public mainChartData1: Array<number> = [];

  public mainChartData: Array<any> = [
    {
      data: this.mainChartData1,
      label: 'Search Volume'
    }
  ];
  /* tslint:disable:max-line-length */
  public mainChartLabels: Array<any> = [];
  /* tslint:enable:max-line-length */
  public mainChartOptions: any;

  public mainChartColours: Array<any> = [
    { // brandInfo
      backgroundColor: hexToRgba(getStyle('--info'), 10),
      borderColor: getStyle('--info'),
      pointHoverBackgroundColor: '#fff'
    }
  ];
  public mainChartLegend = false;
  public mainChartType = 'line';

  getDataKeyWordPlanner(idDomain : number) {
    this.keywordPlanerService.getDataKeyword(idDomain).subscribe(
      response => {
        this.keyWordPlanner = response;
        this.dtOptions = {
          pagingType: 'full_numbers'
        };
        this.dtTrigger.next();
      }
    );

  }

  getDataHistory(idKeyword: number) {
    this.keywordPlanerService.getDataHistory(idKeyword).subscribe(
      response => {
        this.keyWordPlanerHistory = response;
        this.viewDetail = true;
        this.dtOptionsHistory = {
          pagingType: 'full_numbers'
        };
      }
    );
  }
  getDataRelatedKeyword(keyword :string){
    this.keywordPlanerService.getDataRelatedKeyword(keyword).subscribe(
      response => {
        this.relatedKeyword = response
        console.log(this.relatedKeyword);
        this.viewRelatedKeyword = true;
        this.dtOptionsRelatedKeyword = {
          pagingType: 'full_numbers'
        };
      }
    );
  }

  getDataGraph(idKeyword: number) {
    this.keywordPlanerService.getGraphHistory(idKeyword).subscribe(
      response => {
        this.historyGraph = response
        for (let index = 0; index < this.historyGraph.value.length; index++) {
          this.mainChartData1[index] = this.historyGraph.value[index];

        }
        this.mainChartLabels = this.historyGraph.date;
        // this.maxCeil = this.traficAnalystSumary.maxValue;
        this.setProperty(this.historyGraph.maxValue);
        this.chartReady = true;
      }
    );
  }
  getAllinTitleStatus(val: number) {
    try {
      if (val <= 13000) {
        this.warna = 'text-success';
      } else if (val > 13000 && val <= 650000) {
        this.warna = 'text-warning';
      } else if (val > 650000 && val <= 105000) {
        this.warna = 'text-danger';
      } else if (val > 105000) {
        this.warna = 'text-danger';
      }
    } catch (error) {

    }

    // console.log(this.warna);

  }
  viewData(id: number, keyword: string, content, relatedSearch: string) {
    this.keyword = keyword;
    this.relatedSearch = relatedSearch;
    this.openLg(id, content);
    this.getDataGraph(id);
  }
  viewRelated(keyword: string, content){
    this.keyword = keyword;
    this.openLg2(keyword, content);
    
  }


  addDataKeyword(content) {
    this.openSm(content);
  }
  saveDataKeyword() {
    this.keywordPlanerService.addDataKeyword(
      new KeyWordPlaner(-1, this.addKeyword, 0, 0, 0, 0, '-', Number(sessionStorage.getItem('activeDomain')))
    ).subscribe(
      response => {
        console.log(response);
        this.modalService.dismissAll();
        this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
          this.router.navigate(['seo/keyword-planer']));
      }
    );
  }
  deleteKeywordBtn(id: number, content, keyword: string) {
    this.keyword = keyword;
    this.deleteid = id;
    this.openSm(content);
  }
  deleteid = 0;
  deteKeyword() {
    this.keywordPlanerService.removeData(this.deleteid).subscribe(
      response => {
        console.log(response);
        this.modalService.dismissAll();
        this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
          this.router.navigate(['seo/keyword-planer']));
      }
    );
  }

  updateData() {
    this.notInUpdate = false;
    this.keywordPlanerService.updateData(Number(sessionStorage.getItem('activeDomain'))).subscribe(
      response => {
        this.updateStatus = response;
        console.log(response);
        this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
          this.router.navigate(['seo/keyword-planer']));
      }
    );
  }

  openLg(idKeyword: number, content) {
    // this.getRankTrackingLongs(idKeyword)
    this.getDataHistory(idKeyword);
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', windowClass: 'my-class' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      this.viewDetail = false;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.viewDetail = false;
    });
  }
  openLg2(keyword: string, content) {
    // this.getRankTrackingLongs(idKeyword)
    this.getDataRelatedKeyword(keyword);
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', windowClass: 'my-class' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      this.viewRelatedKeyword = false;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.viewRelatedKeyword = false;
    });
  }


  openSm(content) {
    // this.getRankTrackingLongs(idKeyword)
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      this.addData = false;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.addData = false;
    });
  }


  private getDismissReason(reason: any): string {
    // this.dtTriggerLog.unsubscribe();

    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }



  }
  setProperty(maxCeil: number) {
    this.mainChartOptions = {
      tooltips: {
        enabled: false,
        custom: CustomTooltips,
        intersect: true,
        mode: 'index',
        position: 'nearest',
        callbacks: {
          labelColor: function (tooltipItem, chart) {
            return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor };
          }
        }
      },
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        xAxes: [{
          gridLines: {
            drawOnChartArea: false,
          },
          ticks: {
            callback: function (value: any) {
              return value;
            }
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true,
            maxTicksLimit: 5,
            stepSize: Math.ceil(maxCeil / 5),
            max: maxCeil
          }
        }]
      },
      elements: {
        line: {
          borderWidth: 2
        },
        point: {
          radius: 0,
          hitRadius: 10,
          hoverRadius: 4,
          hoverBorderWidth: 3,
        }
      },
      legend: {
        display: false
      }
    }
  }

}
