import { Component, OnInit, AfterViewInit } from '@angular/core';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { TraficAnalystSumary } from '../../../models/traficAnalystSumary.model';
import { TraficAnalystService } from '../../service/data/trafic-analyst.service';
import { formatDate, NgLocalization } from '@angular/common';
import { GoogleConsoleService } from '../../service/data/google-console.service';
import { DashboardSumary } from '../../../models/dashboard.model';
import { BackLinkService } from '../../service/data/back-link.service';
import { concat } from 'rxjs';
import { Router } from '@angular/router';
// import { AuthGuardService } from '../../service/auth/auth-guard.service';

@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit,AfterViewInit {

  chartReady = false;
  nowTime = new Date();
  notInUpdate = true;
  jstoday = '';
  dataDashboard = new DashboardSumary();
  // dataDashboard :any;

  // mainChart
  traficAnalystSumary: TraficAnalystSumary;
  public mainChartElements = 30;
  // maxCeil = 30;
  public mainChartData1: Array<number> = [];
  public mainChartData2: Array<number> = [];
  public mainChartData3: Array<number> = [];
  public mainChartData4: Array<number> = [];

  public mainChartData: Array<any> = [
    {
      data: this.mainChartData1,
      label: 'Trafic'
    },
    {
      data: this.mainChartData2,
      label: 'Click'
    },
    {
      data: this.mainChartData3,
      label: 'CTR(%)'
    },
    {
      data: this.mainChartData4,
      label: 'Average Trafic'
    }
  ];
  /* tslint:disable:max-line-length */
  public mainChartLabels: Array<any> = [];
  /* tslint:enable:max-line-length */
  public mainChartOptions: any;

  public mainChartColours: Array<any> = [
    { // brandInfo
      backgroundColor: hexToRgba(getStyle('--info'), 10),
      borderColor: getStyle('--info'),
      pointHoverBackgroundColor: '#fff'
    },
    { // brandSuccess
      backgroundColor: 'transparent',
      borderColor: getStyle('--success'),
      pointHoverBackgroundColor: '#fff'
    },
    { // brandInfo
      backgroundColor: 'transparent',
      borderColor: getStyle('--warning'),
      pointHoverBackgroundColor: '#fff'
    },
    { // brandDanger
      backgroundColor: 'transparent',
      borderColor: getStyle('--danger'),
      pointHoverBackgroundColor: '#fff',
      borderWidth: 1,
      borderDash: [8, 5]
    }
  ];
  public mainChartLegend = false;
  public mainChartType = 'line';

  getDataSumary() {
    this.traficAnalystService.getSumary(Number(sessionStorage.getItem('activeDomain'))).subscribe(
      response => {
        this.traficAnalystSumary = response;
        for (let index = 0; index < this.traficAnalystSumary.trafic.length; index++) {
          this.mainChartData1.push(this.traficAnalystSumary.trafic[index]);

        }
        for (let index = 0; index < this.traficAnalystSumary.click.length; index++) {
          this.mainChartData2.push(this.traficAnalystSumary.click[index]);
        }
        for (let index = 0; index < this.traficAnalystSumary.ctr.length; index++) {
          this.mainChartData3.push(this.traficAnalystSumary.ctr[index] / 100);
        }
        // this.mainChartData2 = this.traficAnalystSumary.ctr;

        for (let index = 0; index < this.traficAnalystSumary.click.length; index++) {
          this.mainChartData4.push(this.traficAnalystSumary.averageTrafic);
        }
        this.mainChartLabels = this.traficAnalystSumary.dates;
        // this.maxCeil = this.traficAnalystSumary.maxValue;
        this.setProperty(this.traficAnalystSumary.maxValue);
        this.chartReady = true;
        // console.log(this.traficAnalystSumary);
      }
    );
  }

  constructor(
    private traficAnalystService: TraficAnalystService,
    private googleService: GoogleConsoleService,
    private backlinkService: BackLinkService,
    private router : Router
  ) {
    this.jstoday = formatDate(this.nowTime, 'dd-MM-yyyy hh:mm:ss a', 'en-US', '+0700');
  }
  ngOnInit(): void {
      sessionStorage.setItem('activeDomain',String(10009));
      this.getDataDashboard();
      this.getDataSumary();
    
  }
  ngAfterViewInit(){
    
  }
  updateData() {
    this.notInUpdate = false;
    this.googleService.updateData().subscribe(
      response => {
        console.log(response);
        this.backlinkService.updateBackLinkSite(Number(sessionStorage.getItem('activeDomain'))).subscribe(
          response => {
            console.log(response);
            this.backlinkService.updateBackLinkTarget(Number(sessionStorage.getItem('activeDomain'))).subscribe(
              response => {
                console.log(response);
                this.backlinkService.updateBackLinkText(Number(sessionStorage.getItem('activeDomain'))).subscribe(
                  response => {
                    console.log(response);
                    location.reload();
                  }
                );
              }
            );
          }
        );
      }
    );
  }

  getDataDashboard() {
    this.googleService.getDataDashboard(Number(sessionStorage.getItem('activeDomain'))).subscribe(
      response => {
        this.dataDashboard = response;
        
        
      }
    );
  }
  setProperty(maxCeil: number) {
    this.mainChartOptions = {
      tooltips: {
        enabled: false,
        custom: CustomTooltips,
        intersect: true,
        mode: 'index',
        position: 'nearest',
        callbacks: {
          labelColor: function (tooltipItem, chart) {
            return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor };
          }
        }
      },
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        xAxes: [{
          gridLines: {
            drawOnChartArea: false,
          },
          ticks: {
            callback: function (value: any) {
              return value;
            }
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true,
            maxTicksLimit: 5,
            stepSize: Math.ceil(maxCeil / 5),
            max: maxCeil
          }
        }]
      },
      elements: {
        line: {
          borderWidth: 2
        },
        point: {
          radius: 0,
          hitRadius: 10,
          hoverRadius: 4,
          hoverBorderWidth: 3,
        }
      },
      legend: {
        display: false
      }
    }
  };


}
