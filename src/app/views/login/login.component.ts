import { Component, OnInit } from '@angular/core';
import { AuthLogin } from '../../../models/authLogin.model';
import { LoginServiceService } from '../../service/auth/login-service.service';
import { Router } from '@angular/router';
// import { Route } from '@angular/compiler/src/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { AuthGuardService } from '../../service/auth/auth-guard.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit{ 
title = 'appBootstrap';
Message = '';


closeResult: string; 
unautorize = false;  
authData = new AuthLogin();
constructor(
  private authService : LoginServiceService,
  private route : Router,
  private modalService: NgbModal,
  private authGuard : AuthGuardService
  ){

} 


ngOnInit(){
  // sessionStorage.setItem('status',String(false));
  if(this.authGuard.isUserLoggedIn){
    this.route.navigate(['dashboard']);
  }
} 
login(content){
  // console.log(this.authData)
  this.authService.getAuth(this.authData).subscribe(
    auth =>{
      if(auth.Status == 1){
        sessionStorage.setItem('authenticateUser',String(auth.id));
        
       
        this.route.navigate(['dashboard']);
               
      }else{
        this.unautorize = true;
        this.Message = 'Username atau Password Salah'
        this.open(content);
      }
    }
  );
}


open(content) {
  this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
    this.closeResult = `Closed with: ${result}`;
  }, (reason) => {
    this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
}

private getDismissReason(reason: any): string {
  if (reason === ModalDismissReasons.ESC) {
    return 'by pressing ESC';
  } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
    return 'by clicking on a backdrop';
  } else {
    return  `with: ${reason}`;
  }
}

}
