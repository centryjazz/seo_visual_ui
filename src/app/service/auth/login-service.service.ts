import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthLogin } from '../../../models/authLogin.model';
import { StatusResponse } from '../../../models/status.model';

@Injectable({
  providedIn: 'root'
})
export class LoginServiceService {

  constructor(
    private http : HttpClient
  ) { }


  getAuth(data:AuthLogin){
  return this.http.post<StatusResponse>(`http://localhost:9000/auth/`,data);
  }
}
