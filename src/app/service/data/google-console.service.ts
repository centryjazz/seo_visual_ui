import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GoogleConsoleStatus } from '../../../models/googleConsoleStatus.model';
import { DashboardSumary } from '../../../models/dashboard.model';

@Injectable({
  providedIn: 'root'
})
export class GoogleConsoleService {

  constructor(
    private http : HttpClient
  ) { }

 updateData(){
   return this.http.get<GoogleConsoleStatus>(`http://localhost:9000/googleconsole/update`);
 }
 getDataDashboard(idDomain : number){
   return this.http.get<DashboardSumary>(`http://localhost:9000/dashboard/${idDomain}`);
 }
 
}
