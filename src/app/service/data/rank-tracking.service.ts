import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RankTrackingKeyword } from '../../../models/rankTrackingKeyword.model';
import { RankTrackingLog } from '../../../models/rankTrackingLog.model';
import { StatusResponse } from '../../../models/status.model';
import { RankTrackingSumary } from '../../../models/rankTrackingSumary.model';
import { ReqRankTrackingKeywordGraph } from '../../../models/reqGraphRankTracking.model';
import { RankTrackingKeywordGraph } from '../../../models/rankTrackingKeywordGraph.model';
import { TopKeyword } from '../../../models/topKeyWordData.model';

@Injectable({
  providedIn: 'root'
})
export class RankTrackingService {


  constructor(
    private http : HttpClient
  ) { }

  getKeywords(idDomain:number){
    return this.http.get<RankTrackingKeyword[]>(`http://localhost:9000/api/ranktracking/data/keyword/${idDomain}`);
  }
  getLogs(idKeyword:number){
    return this.http.get<RankTrackingLog[]>(`http://localhost:9000/api/ranktracking/data/log/${idKeyword}`)
  }
  getLogsGraph(data: ReqRankTrackingKeywordGraph){
    // console.log(data)
    return this.http.post<RankTrackingLog[]>(`http://localhost:9000/api/ranktracking/keyword/page`,data);
  }
  addKeyword(data : RankTrackingKeyword){
    return this.http.post(`http://localhost:9000/api/ranktracking/data/keyword/add`,data);
  }
//   addData(data){
//     return this.http.post<RankTrackingKeyword>(`http://localhost:9000/api/ranktracking/data/keyword/`,data); 
//  }
  deleteKeyword(idKeyword:number){
    return this.http.delete<StatusResponse>(`http://localhost:9000/api/ranktracking/data/keyword/delete/${idKeyword}`);
  }
  updateKeyword(idDomain:number){
    return this.http.get(`http://localhost:9000/api/ranktracking/update/${idDomain}`);
  }
  getSumary(idDomain:number){
    return this.http.get<RankTrackingSumary>(`http://localhost:9000/api/ranktracking/data/sumary/${idDomain}`);
  }
  getKeywordGraph(data: ReqRankTrackingKeywordGraph){
    return this.http.post<RankTrackingKeywordGraph>(`http://localhost:9000/api/ranktracking/keyword/graph`,data);
  }
  getTopKeyword(idDomain : number){
      return this.http.get<TopKeyword>(`http://localhost:9000/api/ranktracking/data/keyword/top/${idDomain}`);
  }

  getTop3KeywordList(idDomain: number){
      return this.http.get<RankTrackingKeyword[]>(`http://localhost:9000/api/ranktracking/data/keyword/top/3/${idDomain}`);
  }
  getTop5KeywordList(idDomain: number){
      return this.http.get<RankTrackingKeyword[]>(`http://localhost:9000/api/ranktracking/data/keyword/top/5/${idDomain}`);
  }
  getTop10KeywordList(idDomain: number){
    return this.http.get<RankTrackingKeyword[]>(`http://localhost:9000/api/ranktracking/data/keyword/top/10/${idDomain}`);
}
}
