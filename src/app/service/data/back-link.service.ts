import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UpdateStatus } from '../../../models/updateStatus.model';
import { BackLinkText } from '../../../models/backlinkText.model';
import { BackLinkTarget } from '../../../models/backlinkTarget.model';
import { BackLinkSite } from '../../../models/backlinkSite.model';
import { BackLinkSumary } from '../../../models/backlinkSumary.model';

@Injectable({
  providedIn: 'root'
})
export class BackLinkService {

  constructor(
    private http : HttpClient
  ) { }

  getBackLinkSite(idDomain : number){
      return this.http.get<BackLinkSite[]>(`http://localhost:9000/backlink/site/${idDomain}`);
  }
  getBackLinkTarget(idDomain : number){
    return this.http.get<BackLinkTarget[]>(`http://localhost:9000/backlink/targetpage/${idDomain}`);
  }
  getBackLinkText(idDomain : number){
    return this.http.get<BackLinkText[]>(`http://localhost:9000/backlink/text/${idDomain}`);
  }

  updateBackLinkSite(idDomain : number){
    return this.http.get<UpdateStatus>(`http://localhost:9000/backlink/site/update/${idDomain}`);
  }
  updateBackLinkTarget(idDomain : number){
    return this.http.get<UpdateStatus>(`http://localhost:9000/backlink/targetpage/update/${idDomain}`)
  }
  updateBackLinkText(idDomain : number){
    return this.http.get<UpdateStatus>(`http://localhost:9000/backlink/text/update/${idDomain}`);
  }
  getSumary(idDomain : number){
      return this.http.get<BackLinkSumary>(`http://localhost:9000/backlink/sumary/${idDomain}`);
  }
}
