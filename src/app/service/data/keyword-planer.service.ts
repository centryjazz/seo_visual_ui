import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { KeyWordPlaner } from '../../../models/keywordPlaner.model';
import { AttachSession } from 'protractor/built/driverProviders';
import { UpdateStatus } from '../../../models/updateStatus.model';
import { KeyWordPlanerHistory } from '../../../models/keywordPlanerHistory.model';
import { RankTrackingKeywordGraph } from '../../../models/rankTrackingKeywordGraph.model';
import { RelatedKeywords } from '../../../models/RelatedKeyword.model';

@Injectable({
  providedIn: 'root'
})
export class KeywordPlanerService {

  constructor(
    private http : HttpClient
  ) { }

  getDataKeyword(idDomain : number){
    return this.http.get<KeyWordPlaner[]>(`http://localhost:9000/keyword/planner/all/${idDomain}`);
  }
  addDataKeyword(data : KeyWordPlaner){
    return this.http.post<KeyWordPlaner>(`http://localhost:9000/keyword/planner/add`,data);
  }
  removeData(idKeyword :  number){
    return this.http.delete<UpdateStatus>(`http://localhost:9000/keyword/planner/remove/${idKeyword}`);
  }
  getDataHistory(idKeyword : number){
    return this.http.get<KeyWordPlanerHistory[]>(`http://localhost:9000/keyword/planner/history/${idKeyword}`);
  }
  updateData(idDomain : number){
    return this.http.get<UpdateStatus>(`http://localhost:9000/keyword/planner/update/${idDomain}`);
  }
  getGraphHistory(idKeyword : number){
    return this.http.get<RankTrackingKeywordGraph>(`http://localhost:9000/keyword/planner/history/graph/${idKeyword}`);
  }
  getDataRelatedKeyword(keyword : string){
    return this.http.get<RelatedKeywords[]> (`http://localhost:9000/keyword/planner/recomendation/${keyword}`);
  }

}
