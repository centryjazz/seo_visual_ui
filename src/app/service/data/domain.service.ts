import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Domain } from '../../../models/domain.model';
// import { Domain } from 'domain';

@Injectable({
  providedIn: 'root'
})
export class DomainService {

  constructor(
    private http : HttpClient
  ) { }

  getDomain(idUser:number){
    return this.http.get<Domain[]>(`http://localhost:9000/domain/all/${idUser}`);
  }
}
