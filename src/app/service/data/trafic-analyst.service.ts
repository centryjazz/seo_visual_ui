import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TraficPage } from '../../../models/traficAnalystPage.model';
import { RequestPageDetaial } from '../../../models/reqPageTrafic.model';
import { PageTraficDetail } from '../../../models/pageTrafic.model';
import { TraficPageKeyword } from '../../../models/traficAnalystPageKeyword.model';
import { TraficAnalystSumary } from '../../../models/traficAnalystSumary.model';
import { DeviceTrafic } from '../../../models/traficDevice.model';
@Injectable({
  providedIn: 'root'
})
export class TraficAnalystService {

  constructor(
    private http : HttpClient
  ) { }

  getTraficPage(idDomain : number){
    return this.http.get<TraficPage[]>(`http://localhost:9000/traficanalyst/trafic/page/${idDomain}`);
  }
  getDetailPageTrafic(data : RequestPageDetaial){
    return this.http.post<PageTraficDetail[]>(`http://localhost:9000/traficanalyst/trafic/page/detail`,data);
  }
  getTraficPageKeyword(data : RequestPageDetaial){
    return this.http.post<TraficPageKeyword[]>(`http://localhost:9000/traficanalyst/trafic/page/detail/keyword`,data);
  }
  getSumary(idDomain: number){
      return this.http.get<TraficAnalystSumary>(`http://localhost:9000/traficanalyst/trafic/page/sumary/${idDomain}`);
  }
  getDevice(idDomain:number){
    return this.http.get<DeviceTrafic>(`http://localhost:9000/traficanalyst/trafic/page/device/${idDomain}`)
  }
}
