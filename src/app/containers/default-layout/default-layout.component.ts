import {Component, OnInit} from '@angular/core';
import { navItems } from '../../_nav';
import { Router } from '@angular/router';
// import { Domain } from 'domain';
import { DomainService } from '../../service/data/domain.service';
import { Domain } from '../../../models/domain.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent implements OnInit{
  public sidebarMinimized = false;
  public navItems = navItems;
  domain : Domain[] = [];

  constructor(
    private router : Router,
    private domainService : DomainService

  ){

  }
  ngOnInit(){
    this.getDomain();
  }
  
  getDomain(){
    this.domainService.getDomain(Number(sessionStorage.getItem('authenticateUser'))).subscribe(
      response =>{
        this.domain = response;
        // console.log(response);
        sessionStorage.setItem('activeDomain',String(this.domain[0].id));
      }
    );
  }

  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }
  logout(){
    sessionStorage.removeItem('authenticateUser');
    sessionStorage.removeItem('activeDomain');
    this.router.navigate(['login']);
  }
  setDomain(domainName: string){
    let idDomain : number;
    for(let i = 0; i < this.domain.length;i++ ){
      if(this.domain[i].domain === domainName){
        idDomain = this.domain[i].id;
        break;
      }
    }
    sessionStorage.setItem('activeDomain',String(idDomain));
    // console.log('domain id = '+sessionStorage.getItem('activeDomain'));   
  }
}
