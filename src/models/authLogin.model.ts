

export class AuthLogin{
    id : Number;
    username : string;
    password : string;
    name : string;

    constructor(id = -1, username = '', password = '',name = ''){
        this.id = id;
        this.name = name;
        this.username = username;
        this.password = password;
    }
}