export class TraficAnalystSumary{
      maxValue: number;
      dates: number[];
      trafic: number[];
      click: number[];
      ctr: number[];
      averageTrafic: number;
      totalVisit: number;
      totalTrafic: number;
      averageCTR: number;
      totalKeyword: number;
      averagePosition: number;
}