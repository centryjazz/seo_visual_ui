export class RankTrackingSumary{
    
    max_rank : number;
    min_rank : number;
    avg_rank : number;
    unranked : number;
    
}