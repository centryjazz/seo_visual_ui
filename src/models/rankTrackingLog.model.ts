export class RankTrackingLog{
    id : number;
    rank : number;
    page : string;
    date_time : string;
    keyword_id : number;
}