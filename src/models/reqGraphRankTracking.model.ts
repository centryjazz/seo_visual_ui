export class ReqRankTrackingKeywordGraph{
    link : string;
    keyword_id : number;
    
    constructor(link : string, keyword_id : number){
        this.link = link;
        this.keyword_id = keyword_id;
    }
}