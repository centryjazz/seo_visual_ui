export class RelatedKeywords{
    keyword : string;
    avgPosition : number;
    avgImpresion : number;
    constructor(keywords : string, position : number, impresion : number){
        this.keyword = keywords;
        this.avgPosition = position;
        this.avgImpresion = impresion;
    }
}