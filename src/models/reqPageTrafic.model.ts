import { domain } from 'process';

export class RequestPageDetaial{
    pageLink : string;
    domainId : number;
    
    constructor(pageLink = '', domainId = -1){
        this.pageLink = pageLink;
        this.domainId = domainId;
    }
}