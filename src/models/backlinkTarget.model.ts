export class BackLinkTarget{
      id: number;
      target_page: string;
      incoming_link: number;
      linking_site: number;
      domain_id: number;
}