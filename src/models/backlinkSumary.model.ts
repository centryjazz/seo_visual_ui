export class BackLinkSumary{

     totalBackLink: number;
     totalSite: number;
     totalTargetPage: number;
     totalBackLinkKeyword: number;

}