export class KeyWordPlaner{
      id: number ;
      keyword: string ;
      cpc: number ;
      volume: number ;
      competition: number ;
      allinsearch: number ;
      related_search: String ;
      domain_id: number ;
      

      constructor(id = -1,keyword = '', cpc = 0, volume = 0, competition = 0,allinsearch=0,related_search = '', domain_id = -1){
        this.id= id;
        this.keyword = keyword;
        this.cpc = cpc;
        this.volume = volume;
        this.competition = competition;
        this.allinsearch = allinsearch;
        this.related_search = related_search;
        this.domain_id = domain_id;
      }
}