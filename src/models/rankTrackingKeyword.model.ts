export class RankTrackingKeyword{
    id : number;
    keyword : string;
    max_rank : number;
    avg_rank : number;
    min_rank : number;
    last_update : string;
    domain_id : number
    
    constructor(id = -1, keyword = '', max_rank = 0, avg_rank = 0, min_rank = 0, last_update = null,domain_id = -1){
        this.id = id;
        this.keyword = keyword;
        this.max_rank = max_rank;
        this.avg_rank = avg_rank;
        this.min_rank = min_rank;
        this.last_update = last_update;
        this.domain_id = domain_id;
    }
}