export class PageTraficDetail {
    id: number;
    date: Date;
    link: string;
    key: string;
    country: number;
    device: number;
    click: number;
    ctr: number;
    impresion: number;
    position: number;
    domain: number;

    
}