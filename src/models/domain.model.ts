export class Domain{
    id : number;
    domain : string;
    user : number;
    constructor(id = -1, domain = '', user= -1){
        this.id = id;
        this.domain = domain;
        this.user = user;
    }
}