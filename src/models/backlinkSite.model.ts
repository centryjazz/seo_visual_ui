export class BackLinkSite{

      id: number ;
      url: string ;
      linking_page: number ;
      target_page: number ;
      domain_id: number ;
}